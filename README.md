#Frubana_prueba

##Overview
This microservice endpoint order and product

##Requirements

- Spring Framework & Spring Boot
- Gradlew
- Postgres

##Environments
For deployment you has to set the next environment variable (examples)

- DB_HOST=localhost
- DB_PORT=5432
- DB_NAME=frubana
- DB_USER=postgres
- DB_PASSWORD=secret

##Endpoints :
#### 1. GET /orders?search={$search}&value={$value}
List the orders

Request 
```
$search : search by option 'route' ,'order','quantity', 'price' (amount x product price)
$value : value to look for
```

Response
```
[
    {
        "id": 1,
        "productId": 1,
        "properties": [
            {
                "size": "medium"
            }
        ],
        "quantity": 3,
        "orderID": 1,
        "route": {
            "driver": "Pedro",
            "route": 1
        }
    }
]
```

#### 2. GET /product?option={$option}&direction={$direction}
Order products

Request 
```
$option : sort by name or weight. Only option 'name' or 'weight'
$direction : value is 'asc' or 'desc'
```

Response
```
[
    {
        "id": 1,
        "category": "vegetables",
        "owner_name": "corabasto",
        "product_name": "aguacate",
        "name": "Aguacate Hass",
        "properties": [
            {
                "name": "color"
            },
            {
                "color": "light"
            }
        ],
        "weight": 200,
        "price": 300
    },
]
```
