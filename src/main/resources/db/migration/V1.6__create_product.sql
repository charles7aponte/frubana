CREATE TABLE public.product
(
  id SERIAL NOT NULL,
  category character varying(50) ,
  owner_name character varying(50) ,
  product_name character varying(50)  ,
  name character varying(50),
  weight integer ,
  price decimal ,
  CONSTRAINT pk_product PRIMARY KEY (id)
)

