CREATE TABLE public.property_order
(
  id SERIAL NOT NULL,
  name character varying(50) NOT NULL,
  value character varying(50) NOT NULL,
 CONSTRAINT pk_property_order PRIMARY KEY (id)
)