CREATE TABLE public.pivot_property_product
(
  id SERIAL NOT NULL,
  product_id integer ,
  property_id integer ,
 CONSTRAINT pk_pivot_property_product PRIMARY KEY (id),
 CONSTRAINT fk_pp_product FOREIGN KEY (product_id) REFERENCES public.product(id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE RESTRICT,
  CONSTRAINT fk_pp_property FOREIGN KEY (property_id) REFERENCES public.property_product(id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE RESTRICT
)