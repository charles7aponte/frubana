CREATE TABLE public.route_order
(
  id SERIAL NOT NULL,
  drive character varying(50) NOT NULL,
  route integer,
  CONSTRAINT pk_route_order PRIMARY KEY (id)
)