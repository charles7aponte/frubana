CREATE TABLE public.property_product
(
  id SERIAL NOT NULL,
  name character varying(50) NOT NULL,
  value character varying(50) NOT NULL,
 CONSTRAINT pk_property_product PRIMARY KEY (id)
)