CREATE TABLE public.order
(
  id SERIAL NOT NULL,
  product_id integer ,
  quality integer ,
  order_id integer ,
  route_id integer,
 CONSTRAINT pk_order PRIMARY KEY (id),
 CONSTRAINT fk_order_route FOREIGN KEY (route_id) REFERENCES route_order(id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE RESTRICT
)