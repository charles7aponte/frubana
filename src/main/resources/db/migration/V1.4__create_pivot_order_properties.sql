CREATE TABLE public.pivot_property_order
(
  id SERIAL NOT NULL,
  order_id integer ,
  property_id integer ,
 CONSTRAINT pk_pivot_property_order PRIMARY KEY (id),
 CONSTRAINT fk_p_order FOREIGN KEY (order_id) REFERENCES public.order(id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE RESTRICT,
  CONSTRAINT fk_p_property FOREIGN KEY (property_id) REFERENCES public.property_order(id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE RESTRICT
)