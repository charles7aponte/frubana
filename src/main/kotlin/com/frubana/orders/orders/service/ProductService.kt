package com.frubana.orders.orders.service

import com.frubana.orders.orders.repository.ProductRepository
import com.frubana.orders.orders.repository.entity.Product
import com.frubana.orders.orders.utils.PRODUCT_DIRECT_ASC
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service

@Service
class ProductService{

    @Autowired
    private lateinit var productRepository: ProductRepository

    fun getAllOrderBy(order:String, direction:String  ) :List<Product> {

        val direction = when(direction){
             PRODUCT_DIRECT_ASC -> Sort.Direction.ASC
            else ->Sort.Direction.DESC
        }

        return productRepository.findAll(Sort(direction, order))
    }
}