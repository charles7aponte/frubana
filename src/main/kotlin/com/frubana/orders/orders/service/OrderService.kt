package com.frubana.orders.orders.service

import com.frubana.orders.orders.repository.OrderRepository
import com.frubana.orders.orders.repository.entity.Order
import com.frubana.orders.orders.utils.ORDER_ID
import com.frubana.orders.orders.utils.ORDER_PRICE
import com.frubana.orders.orders.utils.ORDER_QUANTITY
import com.frubana.orders.orders.utils.ORDER_ROUTE
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class  OrderService{

    @Autowired
    private lateinit var orderRepository: OrderRepository

    fun getFactorySql(option: String , value: String ) : List<Order>{

        return when(option){
            ORDER_ID -> orderRepository.getAllOrderId(value.toInt())
            ORDER_ROUTE -> orderRepository.getAllRoute(value.toInt())
            ORDER_QUANTITY -> orderRepository.getAllQuality(value.toInt())
            ORDER_PRICE -> orderRepository.getAllPrice(value.toBigDecimal())
           else ->  orderRepository.findAll()
        }
    }
}