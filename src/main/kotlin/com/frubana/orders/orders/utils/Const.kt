package com.frubana.orders.orders.utils

const val  PRODUCT_ORDER_OP1 =  "name"
const val  PRODUCT_ORDER_OP2 =  "weight"
const val  PRODUCT_DIRECT_ASC =  "asc"
const val  PRODUCT_DIRECT_DESC =  "desc"

const val  ORDER_ROUTE =  "route"
const val  ORDER_ID =  "order"
const val  ORDER_QUANTITY =  "quantity"
const val  ORDER_PRICE =  "price"
