package com.frubana.orders.orders.mapper

import com.frubana.orders.orders.mapper.entity.RouteOrderResponse
import com.frubana.orders.orders.mapper.entity.OrderResponse
import com.frubana.orders.orders.repository.entity.Order
import com.frubana.orders.orders.repository.entity.RouteOrder
import org.springframework.stereotype.Component

@Component
class OrderMapper{

    fun  mapperOrderResponse(order:Order): OrderResponse {
        val propeties = ArrayList<HashMap<String , String>>()

        order.properties.map {
            propeties.add(hashMapOf(
                    it.name to it.value
            ))
        }

        return OrderResponse(
                id = order.id,
                productId = order.product?.id ?: 0,
                properties = propeties,
                quantity = order.quality,
                orderID = order.order_id,
                route = this.mapperRoute(order.route)

        )

    }

    fun mapperRoute(route : RouteOrder?): RouteOrderResponse?{
     return  if(route!=null) RouteOrderResponse(driver = route.drive, route = route.route) else null
    }

    fun listMapperOrderResponse(orders : List<Order>): List<OrderResponse>{
       return  orders.map { this.mapperOrderResponse(it) }
    }
}
