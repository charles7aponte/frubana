package com.frubana.orders.orders.mapper

import com.frubana.orders.orders.mapper.entity.ProductResponse
import com.frubana.orders.orders.repository.entity.Product
import org.springframework.stereotype.Component

@Component
class ProductMapper{

    fun  mapperProductResponse(product:Product): ProductResponse {
        val propeties = ArrayList<HashMap<String , String>>()

        product.properties.map {
            propeties.add(hashMapOf(
                    it.name to it.value
            ))
        }

        return ProductResponse(
                id = product.id,
                ownerName = product.ownerName,
                category = product.category,
                productName = product.productName,
                name = product.name,
                properties = propeties,
                weight = product.weight,
                price = product.price
        )

    }

    fun listMapperProductResponse(products : List<Product>): List<ProductResponse>{
       return  products.map { this.mapperProductResponse(it) }
    }
}
