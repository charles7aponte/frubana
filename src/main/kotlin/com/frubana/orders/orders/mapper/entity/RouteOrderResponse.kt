package com.frubana.orders.orders.mapper.entity

class RouteOrderResponse(

        val driver : String,
        val route : Int
)