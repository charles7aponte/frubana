package com.frubana.orders.orders.mapper.entity

import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

class  ProductResponse(
        val id:Int,
        val category:String,

        @get:JsonProperty(value = "owner_name")
        val ownerName :String,

        @get:JsonProperty(value = "product_name")
        val productName:String,
        val name:String,
        val properties: ArrayList<HashMap<String, String>>,
        val weight:Int,
        val price:BigDecimal
)
