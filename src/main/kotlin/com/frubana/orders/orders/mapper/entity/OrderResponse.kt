package com.frubana.orders.orders.mapper.entity

class  OrderResponse(
        val id: Int,
        val productId: Int,
        val properties: ArrayList<HashMap<String, String>>,
        val quantity: Int,
        val orderID: Int,
        val route: RouteOrderResponse?
)