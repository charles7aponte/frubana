package com.frubana.orders.orders.controller

import com.frubana.orders.orders.mapper.OrderMapper
import com.frubana.orders.orders.mapper.entity.OrderResponse
import com.frubana.orders.orders.service.OrderService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class  OrderController{

    @Autowired
    private lateinit var orderService: OrderService

    @Autowired
    private lateinit var orderMapper: OrderMapper

    @GetMapping(Route.ORDER)
    fun getByProperty(@RequestParam("search")  search : String, @RequestParam("value")  value: String  ): List<OrderResponse> {
        return orderMapper.listMapperOrderResponse(orderService.getFactorySql(option = search , value = value ))
    }
}