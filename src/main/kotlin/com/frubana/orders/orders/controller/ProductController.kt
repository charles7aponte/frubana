package com.frubana.orders.orders.controller

import com.frubana.orders.orders.mapper.ProductMapper
import com.frubana.orders.orders.mapper.entity.ProductResponse
import com.frubana.orders.orders.service.ProductService
import com.frubana.orders.orders.utils.PRODUCT_DIRECT_ASC
import com.frubana.orders.orders.utils.PRODUCT_ORDER_OP1
import org.intellij.lang.annotations.Pattern
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class  ProductController{

    @Autowired
    private lateinit var productService: ProductService

    @Autowired
    private lateinit var productMapper: ProductMapper

    @GetMapping(Route.PRODUCT)
    fun getAll(@RequestParam("order") @Pattern( "(weight)") order:String = PRODUCT_ORDER_OP1, @RequestParam("direction") direction:String = PRODUCT_DIRECT_ASC ) : List<ProductResponse> {

       return productMapper.listMapperProductResponse(productService.getAllOrderBy(order = order , direction = direction))
    }

}