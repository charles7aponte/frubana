package com.frubana.orders.orders.repository.sql



const val QUERY_ORDER_ROUTE = """
    SELECT o
    FROM Order o
    WHERE route_id = :routeId

"""

const val QUERY_ORDER_ID = """
    SELECT o
    FROM Order o
    WHERE order_id = :orderId

"""

const val QUERY_ORDER_QUALITY_ = """
    SELECT o
    FROM Order o
    WHERE quality = :quality

"""

const val QUERY_ORDER_PRICE = """
    SELECT o
    FROM Order o
    WHERE o.quality * o.product.price  = :price

"""