package com.frubana.orders.orders.repository

class SearchCriteria (
     val key: String = "",
     val operation: String = "=",
     val value: Any = Any()
)
