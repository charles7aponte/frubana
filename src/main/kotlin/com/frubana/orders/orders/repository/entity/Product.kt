package com.frubana.orders.orders.repository.entity

import javafx.scene.text.FontWeight
import java.math.BigDecimal
import javax.persistence.*

@Entity
@Table(name = "product")
class Product(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        var category: String = "",

        @Column(name = "owner_name")
        var ownerName: String = "",

        @Column(name = "product_name")
        var productName: String = "",

        var name: String = "",

        var weight: Int = 0,

        var price: BigDecimal = BigDecimal.ZERO,

       @ManyToMany
        @JoinTable(
                name = "pivot_property_product",
                joinColumns = arrayOf(JoinColumn(name = "product_id", referencedColumnName = "id")),
                inverseJoinColumns = arrayOf(JoinColumn(name = "property_id", referencedColumnName = "id")))
        val properties: List<PropertyProduct> = listOf()
)