package com.frubana.orders.orders.repository.entity

import javax.persistence.*

@Entity
@Table(name = "route_order")
class RouteOrder(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        var drive: String = "",

        var route: Int = 0
)