package com.frubana.orders.orders.repository.entity

import javax.persistence.*

@Entity
@Table(name = "property_product")
class PropertyProduct(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        var name: String = "",

        var value: String = ""
)