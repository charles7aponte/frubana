package com.frubana.orders.orders.repository.entity

import javax.persistence.*

@Entity
@Table(name = "orders")
class Order(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        @Column(name = "order_id")
        var order_id: Int = 0,

        var  quality: Int = 0,

        @JoinColumn(name = "product_id", referencedColumnName = "id")
        @ManyToOne(optional = false, fetch = FetchType.EAGER)
        var product: Product? = null,

        @JoinColumn(name = "route_id", referencedColumnName = "id")
        @ManyToOne(optional = false, fetch = FetchType.EAGER)
        var route: RouteOrder? = null,

       @ManyToMany
        @JoinTable(
                name = "pivot_property_order",
                joinColumns = arrayOf(JoinColumn(name = "order_id", referencedColumnName = "id")),
                inverseJoinColumns = arrayOf(JoinColumn(name = "property_id", referencedColumnName = "id")))
        var properties: List<PropertyOrder> = listOf()
)