package com.frubana.orders.orders.repository

import com.frubana.orders.orders.repository.entity.Product
import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.*

class ProductSpecification(
        var criteria: SearchCriteria = SearchCriteria()
    ) : Specification<Product>
{

    override fun toPredicate(root: Root<Product>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        val name  = root.get<Product>(criteria.key)
              return   builder.equal(name, criteria.value)

    }
}