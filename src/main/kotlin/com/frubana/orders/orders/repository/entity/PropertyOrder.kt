package com.frubana.orders.orders.repository.entity

import javax.persistence.*

@Entity
@Table(name = "property_order")
class PropertyOrder(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,

        val name: String = "",

        val value: String = ""
)