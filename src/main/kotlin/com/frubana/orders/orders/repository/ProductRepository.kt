package com.frubana.orders.orders.repository

import org.springframework.data.jpa.repository.JpaRepository
import com.frubana.orders.orders.repository.entity.Product
import com.frubana.orders.orders.repository.sql.QUERY_PRODUCT_ALL
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import javax.persistence.EntityManager


@Repository
interface ProductRepository : JpaRepository<Product, Int>, JpaSpecificationExecutor<Product> {

    @Query(QUERY_PRODUCT_ALL)
    fun all(): List<Product>

    override fun findAll(sort: Sort): List<Product>

}