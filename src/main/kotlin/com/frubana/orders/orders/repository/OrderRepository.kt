package com.frubana.orders.orders.repository

import org.springframework.data.jpa.repository.JpaRepository
import com.frubana.orders.orders.repository.entity.Order
import com.frubana.orders.orders.repository.sql.QUERY_ORDER_ID
import com.frubana.orders.orders.repository.sql.QUERY_ORDER_PRICE
import com.frubana.orders.orders.repository.sql.QUERY_ORDER_QUALITY_
import com.frubana.orders.orders.repository.sql.QUERY_ORDER_ROUTE
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.math.BigDecimal


@Repository
interface  OrderRepository : JpaRepository<Order,Int>
{
    override fun findAll(): List<Order>

    @Query(QUERY_ORDER_ROUTE)
    fun getAllRoute(@Param("routeId") routeId: Int): List<Order>

    @Query(QUERY_ORDER_ID)
    fun getAllOrderId(@Param("orderId") orderId: Int): List<Order>

    @Query(QUERY_ORDER_QUALITY_)
    fun getAllQuality(@Param("quality") quality: Int): List<Order>

    @Query(QUERY_ORDER_PRICE)
    fun getAllPrice(@Param("price") quality: BigDecimal): List<Order>

}