package com.frubana.orders.orders.controller

import com.frubana.orders.orders.repository.OrderRepository
import com.frubana.orders.orders.repository.ProductRepository
import com.frubana.orders.orders.repository.entity.Order
import com.frubana.orders.orders.repository.entity.Product
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.transaction.annotation.Transactional
import org.junit.Before

@ActiveProfiles("local")
@RunWith(SpringRunner::class)
@AutoConfigureMockMvc
@SpringBootTest
@Transactional
class  ProductControllerTest{

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var productRepository: ProductRepository

    private val option : String = "weight"
    private  val direction : String = "asc"

    @Before
    fun setup(){
        productRepository.deleteAll()
        productRepository.save(Product(id = 3,weight = 2))
        productRepository.save(Product(id = 4,weight = 1))

    }

    @Test
    fun `test product`(){

        mockMvc.perform(MockMvcRequestBuilders.get("/${Route.PRODUCT}?order=${option}&direction=${direction}"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].weight").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].weight").value(2))
    }
}