package com.frubana.orders.orders.controller

import com.frubana.orders.orders.repository.OrderRepository
import com.frubana.orders.orders.repository.entity.Order
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.transaction.annotation.Transactional
import org.junit.Before

@ActiveProfiles("local")
@RunWith(SpringRunner::class)
@AutoConfigureMockMvc
@SpringBootTest
@Transactional
class  OrderControllerTest{

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var orderRepository: OrderRepository

    private val option : String = "quantity"
    private  val value : String = "300"

    @Before
    fun setup(){
        orderRepository.deleteAll()
        orderRepository.save(Order(order_id = 4,quality = value.toInt()))
    }

    @Test
    fun `test orders`(){

        mockMvc.perform(MockMvcRequestBuilders.get("/${Route.ORDER}?search=${option}&value=${value}"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].quantity").value(value))

    }
}